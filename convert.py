import sys

import bs4


def main(argv):
    for a in argv[1:]:
        with open(a, 'r', encoding='ISO-8859-1') as file:
            txt = file.read()
            soup = bs4.BeautifulSoup(txt, features='html.parser')

        new_link = soup.new_tag('link', rel='stylesheet', type='text/css', href='../prettify.css')
        soup.head.append(new_link)

        new_link = soup.new_tag('link', rel='stylesheet', type='text/css', href='../sons-of-obsidian.css')
        soup.head.append(new_link)

        new_link = soup.new_tag('script', type='text/javascript', src='../prettify.js')
        soup.head.append(new_link)

        soup.find('body')['onload'] = 'PR.prettyPrint()'

        for pre in soup.findAll('pre', {'class': 'PROGRAMLISTING'}):
            pre['class'] = 'prettyprint linenums'

        with open(a, 'w') as outf:
            outf.write(str(soup))


if __name__ == '__main__':
    main(sys.argv)
